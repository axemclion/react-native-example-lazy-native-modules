import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableHighlight,
  Button
} from "react-native";

let _deviceModuleInfo = null;
function getDeviceModuleInfo() {
  if (_deviceModuleInfo === null) {
    _deviceModuleInfo = require("react-native-device-info").default;
  }
  return _deviceModuleInfo;
}

export default class App extends Component {
  state = { values: {}, methods: [] };
  _handlePress(item) {
    let result;
    try {
      result = getDeviceModuleInfo()[item]();
    } catch (err) {
      result = err;
    }
    this.setState(({ values }) => ({
      values: {
        ...values,
        [item]: result
      }
    }));
  }

  render() {
    return (
      <View>
        <Button
          onPress={() =>
            this.setState({ methods: Object.keys(getDeviceModuleInfo()) })
          }
          title="Load Device Info Module"
        />
        <FlatList
          data={this.state.methods}
          keyExtractor={item => item}
          extraData={this.state.values}
          renderItem={({ item }) => (
            <TouchableHighlight onPress={() => this._handlePress(item)}>
              <View style={styles.row}>
                <Text style={styles.key}>{item}</Text>
                <Text style={styles.value}>
                  {JSON.stringify(this.state.values[item])}
                </Text>
              </View>
            </TouchableHighlight>
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flex: 1,
    flexDirection: "row",
    padding: 3,
    margin: 3,
    justifyContent: "space-between"
  },
  key: {
    color: "#555"
  },
  value: {
    color: "#999",
    backgroundColor: "#eee"
  }
});
